package config

const (
	ResolveMethodProxy      = "proxy"
	ResolveMethodRemoteAddr = "remote"
)
