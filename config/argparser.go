package config

import (
	"fmt"
	"github.com/akamensky/argparse"
	"os"
)

type Args struct {
	ConfigLocation string
	Listen         string
	PromPort       string
	RateLimit      string
}

func parseArgs() (*Args, error) {
	parser := argparse.NewParser("goma", "goma")

	configLocation := parser.String("c", "config", &argparse.Options{Required: false, Default: "", Help: "Specifies a configuration to load"})
	port := parser.String("l", "listen", &argparse.Options{Required: false, Default: ":8080", Help: "Defines which port to use"})
	promPort := parser.String("p", "promPort", &argparse.Options{Required: false, Default: ":9090", Help: "Defines which port to use for metrics"})
	rateLimit := parser.String("r", "ratelimit", &argparse.Options{Required: false, Default: "4-m", Help: "Defines the rate limit"})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		return nil, err
	}

	args := &Args{
		ConfigLocation: *configLocation,
		Listen:         *port,
		PromPort:       *promPort,
		RateLimit:      *rateLimit,
	}

	return args, nil
}
