package config

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
)

type User struct {
	User   string `json:"user"`
	Secret string `json:"secret"`
}

type Config struct {
	Users []struct {
		User   string `json:"user"`
		Secret string `json:"secret"`
	} `json:"users"`
	Cmd            []string `json:"cmd"`
	IpResolver     string   `json:"ipresolver"`
	TimeoutSeconds int      `json:"timeoutSeconds"`
	Undo           []string `json:"undo"`
	Listen         string   `json:"listen"`
	PromListen     string   `json:"promListen"`
	RateLimit      string   `json:"rateLimit"`
	Logfile        string   `json:logfile`
}

func NewConfig(args *Args) *Config {
	if args == nil {
		return &Config{}
	}

	return &Config{
		RateLimit:  args.RateLimit,
		PromListen: args.PromPort,
		Listen:     args.Listen,
	}
}

// validate checks whether the config object is considered valid.
func (c *Config) validate() error {
	if len(c.Users) < 1 {
		return fmt.Errorf("no user(s) configured")
	}

	for _, user := range c.Users {
		if len(user.User) < 3 {
			return fmt.Errorf("invalid username: %s", user.User)
		}

		if len(user.Secret) != 16 {
			return fmt.Errorf("secret does not meet conditions, length = %d", len(user.Secret))
		}
	}

	type void struct{}
	visited := make(map[string]void)
	for _, user := range c.Users {
		if _, ok := visited[user.User]; ok {
			return fmt.Errorf("duplicated user defined: %s", user.User)
		}
		visited[user.User] = void{}
	}

	if nil == c.Cmd {
		return fmt.Errorf("cmd not defined")
	}

	if len(c.Cmd) < 2 {
		return fmt.Errorf("cmd does not consist of command and parameter(s)")
	}

	if c.IpResolver != ResolveMethodProxy && c.IpResolver != ResolveMethodRemoteAddr {
		return fmt.Errorf("only '%s' and '%s' are allowed for ipResolver", ResolveMethodProxy, ResolveMethodRemoteAddr)
	}

	if c.TimeoutSeconds < 1 {
		return fmt.Errorf("timeout must be greather than 0")
	}

	if len(c.Listen) == 0 {
		return fmt.Errorf("listen must not be empty")
	}

	if len(c.PromListen) == 0 {
		return fmt.Errorf("promListen must not be empty")
	}

	return nil
}

// BuildConfig builds and validates a config object. If no config could be found at any of the given locations
// or if the validation failed, a nil pointer and an error is returned.
func BuildConfig() (*Config, error) {
	args, err := parseArgs()
	if err != nil {
		log.Fatal(err)
	}

	var locations []string
	if len(args.ConfigLocation) > 0 {
		locations = append(locations, args.ConfigLocation)
	}

	home, _ := os.UserHomeDir()
	var configLocations = []string{"/etc/goma/config.json", home + "/.config/goma/config.json"}
	for _, location := range configLocations {
		locations = append(locations, location)
	}

	return read(locations, args)
}

func (c *Config) LogInfo() {
	log.Infof("Loaded %d users from config", len(c.Users))
	log.Infof("Rate-limit set to '%s'", c.RateLimit)
	log.Infof("Using ip resolving strategy '%s'", c.IpResolver)
	log.Infof("Using cmd: %v", c.Cmd)
}

// read tries to succesively read a config from multiple locations.
func read(locations []string, args *Args) (*Config, error) {
	for _, location := range locations {
		log.Infof("Trying to read config from location %s", location)
		conf, err := loadAndValidateConfig(location, args)
		if err == nil {
			log.Info("Successfully read config")
			return conf, nil
		}

		log.Errorf("Failed to use config: %s", err)
	}

	return nil, fmt.Errorf("did not find a valid config at locations: %v", locations)
}

func loadAndValidateConfig(location string, args *Args) (*Config, error) {
	content, err := ioutil.ReadFile(location)

	if err == nil {
		c := NewConfig(args)
		err := json.Unmarshal(content, &c)
		if err == nil {
			if valError := c.validate(); valError == nil {
				return c, nil
			} else {
				return nil, fmt.Errorf("invalid config at %s: %s", location, valError.Error())
			}
		}
	}

	return nil, fmt.Errorf("no (valid) config found at %s", location)
}
