module goma

go 1.13

require (
	github.com/akamensky/argparse v1.2.1
	github.com/prometheus/client_golang v1.5.1
	github.com/sirupsen/logrus v1.5.0
	github.com/ulule/limiter/v3 v3.4.2
	github.com/xlzd/gotp v0.0.0-20181030022105-c8557ba2c119
)
