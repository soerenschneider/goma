package main

import (
	log "github.com/sirupsen/logrus"
	limiter "github.com/ulule/limiter/v3"
	mhttp "github.com/ulule/limiter/v3/drivers/middleware/stdlib"
	"github.com/ulule/limiter/v3/drivers/store/memory"
	"goma/config"
	"goma/handler"
	"goma/metrics"
	"net/http"
	"os"
)

func main() {
	log.Info("Starting goma...")

	conf, err := config.BuildConfig()
	if conf == nil || err != nil {
		log.Fatal("Could not read config")
	}

	if len(conf.Logfile) > 0 {
		logfile, err := os.OpenFile(conf.Logfile, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
		if err != nil {
			log.Fatalf("Error opening logfile: %v", err)
		}
		defer logfile.Close()
		log.SetOutput(logfile)
	}

	conf.LogInfo()

	middleware, err := buildMiddleware(conf.RateLimit, conf.IpResolver == config.ResolveMethodProxy)
	if err != nil {
		log.Fatalf("could not create middleware: %s", err.Error())
	}

	runtime := handler.BuildRuntime(conf)

	go metrics.StartProm(conf.PromListen)

	http.Handle("/", middleware.Handler(http.HandlerFunc(runtime.Goma)))
	err = http.ListenAndServe(conf.Listen, nil)
	if err != nil {
		log.Fatalf("Could not start http server: %s", err.Error())
	}
}

func buildMiddleware(rateLimit string, useProxy bool) (*mhttp.Middleware, error) {
	rate, err := limiter.NewRateFromFormatted(rateLimit)
	if err != nil {
		return nil, err
	}

	store := memory.NewStore()

	var limit *limiter.Limiter
	if useProxy {
		limit = limiter.New(store, rate, limiter.WithTrustForwardHeader(true))
	} else {
		limit = limiter.New(store, rate, limiter.WithTrustForwardHeader(false))
	}

	return mhttp.NewMiddleware(limit), nil
}
