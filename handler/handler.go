package handler

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/xlzd/gotp"
	"goma/config"
	"goma/metrics"
	"html/template"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path"
)

var (
	inputTemplate *template.Template
	successTemplate *template.Template
)

type UserInput struct {
	user string
	code string
}

func (u *UserInput) validateRequest() error {
	if len(u.code) != 6 {
		return fmt.Errorf("invalid code length")
	}

	if len(u.user) < 3 {
		return fmt.Errorf("no user supplied")
	}
	
	return nil
}

type Response struct {
	Failed bool
}

type Runtime struct {
	users    map[string]*gotp.TOTP
	resolver IpResolver
	cmd      string
	cmdArgs  []string
}

func BuildRuntime(conf *config.Config) Runtime {
	if initTemplates() != nil {
		log.Fatalf("could not find any templates")
	}
	
	runtime := Runtime{}
	
	if conf.IpResolver == config.ResolveMethodProxy {
		runtime.resolver = &ProxyIpResolver{}
	} else {
		runtime.resolver = &RemoteAddressIpResolver{}
	}

	runtime.users = make(map[string]*gotp.TOTP)
	for _, user := range conf.Users {
		runtime.users[user.User] = buildTotp(user.Secret)
	}

	runtime.cmd = conf.Cmd[0]
	runtime.cmdArgs = conf.Cmd[1:]

	return runtime
}

func initTemplates() error {
	locations := determineTemplateDirs()
	
	for _, location := range locations {
		log.Infof("Trying to find templates in %s", location)
		if _, err := os.Stat(location); !os.IsNotExist(err) {
			_, inputTemplateErr := os.Stat(path.Join(location, "input.html"))
			_, successTemplateErr := os.Stat(path.Join(location, "success.html"))

			if !os.IsNotExist(inputTemplateErr) && !os.IsNotExist(successTemplateErr){
				inputTemplate = template.Must(template.ParseFiles(path.Join(location, "input.html")))
				successTemplate = template.Must(template.ParseFiles(path.Join(location, "success.html")))

				log.Info("Successfully loaded templates")
				return nil
			}
		}
	}
	
	return errors.New("could not find templates at any directory")
}

func determineTemplateDirs() []string {
	var locations []string

	templatesDirectory := os.Getenv("GOMA_TEMPLATES")
	if len(templatesDirectory) == 0 {
		templatesDirectory = "templates"
	}
	locations = append(locations, templatesDirectory)
	var configLocations = []string{"/etc/goma/templates"}
	for _, location := range configLocations {
		locations = append(locations, location)
	}
	
	return locations
}

func (c *Runtime) check(user string, code string) bool {
	val, found := c.users[user]
	if !found {
		return false
	}

	return val.Now() == code
}

func buildTotp(secret string) *gotp.TOTP {
	if len(secret) == 0 {
		panic("No secret configured")
	}

	totp := gotp.NewDefaultTOTP(secret)
	totp.Now()
	return totp
}

func (runtime *Runtime) Goma(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		metrics.PromRequests.WithLabelValues("GET").Inc()
		inputTemplate.Execute(w, nil)
		return
	}

	metrics.PromRequests.WithLabelValues("POST").Inc()
	request := UserInput{
		user: r.FormValue("user"),
		code: r.FormValue("code"),
	}
	
	if request.validateRequest() != nil {
		metrics.PromInvalidRequests.Inc()
		inputTemplate.Execute(w, Response{
			Failed: true,
		})
		return
	}

	ip, _ := runtime.GetIP(r)
	if len(ip) == 0 {
		log.Error("Could not extract IP")
		metrics.PromIpExtractionError.Inc()
		return
	}

	success := runtime.checkCode(request.user, request.code, ip)
	if success {
		resp := struct{
			Ip string
		} {
			Ip: ip,
		}
		successTemplate.Execute(w, resp)
	} else {
		inputTemplate.Execute(w, Response{
			Failed: true,
		})
	}
}

func (runtime *Runtime) checkCode(user, code, ip string) bool {
	success := runtime.check(user, code)

	if success {
		metrics.PromSuccessfulAuthentications.WithLabelValues(user).Inc()
		runtime.execute(ip)
		return true
	}

	metrics.PromAuthenticationFailures.WithLabelValues(user).Inc()
	log.Infof("Invalid attempt from %s", ip)
	return false
}

func (c *Runtime) execute(ip string) {
	args := substituteIp(c.cmdArgs, ip)
	log.Infof("Executing %s %v", c.cmd, args)
	cmd := exec.Command(c.cmd, args...)
	_, err := cmd.Output()

	if err != nil {
		metrics.PromCommandErrors.Inc()
		log.Errorf("error while running command: ", err.Error())
	}
}

func substituteIp(orig []string, ip string) []string {
	var args []string
	for _, entry := range orig {
		if entry == "$IP" {
			entry = ip
		}
		args = append(args, entry)
	}
	return args
}

func (runtime *Runtime) GetIP(r *http.Request) (string, error) {
	raw := runtime.resolver.ResolveIp(r)
	ip, _, err := net.SplitHostPort(raw)
	return ip, err
}
