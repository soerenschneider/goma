package handler

import (
	"net/http"
)


type IpResolver interface {
	ResolveIp(r *http.Request) string
}

type RemoteAddressIpResolver struct {
}

func (p *RemoteAddressIpResolver) ResolveIp(r *http.Request) string {
	return r.RemoteAddr
}

type ProxyIpResolver struct {
}

func (p *ProxyIpResolver) ResolveIp(r *http.Request) string {
	return r.Header.Get("X-FORWARDED-FOR")
}