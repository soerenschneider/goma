package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"net/http"
)

var (
	PromRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "goma_authentication_requests_total",
		Help: "The total number of processed requests",
	}, []string{"method"})

	PromInvalidRequests = promauto.NewCounter(prometheus.CounterOpts{
		Name: "goma_invalid_requests_total",
		Help: "The total number of invalid requests",
	})

	PromSuccessfulAuthentications = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "goma_authentication_success_total",
		Help: "The total number of successful authentications",
	}, []string{"user"})

	PromAuthenticationFailures = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "goma_authentication_failures_total",
		Help: "The total number of failed authentications",
	}, []string{"user"})

	PromCommandErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "goma_command_errors_total",
		Help: "The total number of failed executed commands",
	})

	PromIpExtractionError = promauto.NewCounter(prometheus.CounterOpts{
		Name: "goma_ipextraction_errors_total",
		Help: "The total number of failures retrieving the client's IP",
	})
)

func StartProm(listen string) {
	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(listen, nil)
	if err != nil {
		log.Fatalf("Could not start prometheus http server: %s", err.Error)
	}
}
